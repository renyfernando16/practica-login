package com.example.renydelgado;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText txtUsuario;
    EditText txtcontrasena;
    Button btnIngresar;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnIngresar = (Button) findViewById(R.id.btnIngresar);

        txtUsuario = (EditText) findViewById(R.id.txtUsuario);
        txtcontrasena = (EditText)findViewById(R.id.txtContrasena);



        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Llamar a nueva pantalla
                Intent i = new Intent(MainActivity.this, login2.class);
                startActivity(i);

            }
        });


    }
}
